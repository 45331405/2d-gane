﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    public Transform player;        //Public variable to store a reference to the player game object
    public float boundX = 2.0f;

    public float speed;
    private Vector3 CamPos;            //Private variable to store the offset distance between the player and camera
    Vector3 offset;
    public float offsetY;
    public float offsetZ;
    // Use this for initialization
    void Start () {
    }

    // Update is called once per frame
    void LateUpdate()
    {
       Vector3 delta = Vector3.zero;

        float dx = player.position.x - transform.position.x;

        if (dx > boundX || dx < -boundX)
        {
            if (transform.position.x < player.position.x)
            {
                delta.x = dx - boundX;
            }
            else
            {
                delta.x = dx + boundX;
            }
        }
        delta.z = -10;  
        // Set the position of the camera's transform to be the same as the player's, but offset by the calculated offset distance.
        CamPos = player.transform.position + delta;

        offset = new Vector3(CamPos.x, CamPos.y + offsetY, offsetZ);
        transform.position = Vector3.Lerp(transform.position, offset, speed);
    }
}
