﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class ShellMenu : MonoBehaviour {

    public GameObject info;
    //front panel
    public GameObject shellPanel;

    // game over
    public GameObject gameover; 
    //game ui
    public Slider playerHealth;
    public movement playerref;
    public Text score;
    public float timer;

    private bool paused = true;

    // options
    public GameObject optionsPanel;
    public Dropdown qualityDropdown;
    public Dropdown resolutionDropdown;
    public Toggle fullScreenToggle;
    public Slider volumeSlider;


    public AudioSource MusicSource;
    public Slider MusicSlider;


    // Use this for initialization
    void Start () {

        info.SetActive(true);
        playerref = GameObject.FindObjectOfType<movement>();

        MusicSource = MusicSource.GetComponent<AudioSource>();
        MusicSource.ignoreListenerVolume = true;

        optionsPanel.SetActive(false);
        SetPaused(paused);

        // populate the list of video quality levels
        qualityDropdown.ClearOptions();
        List<string> names = new List<string>();
        for (int i = 0; i < QualitySettings.names.Length; i++)
        {
            names.Add(QualitySettings.names[i]);
        }
        qualityDropdown.AddOptions(names);


        // populate the list of available resolutions
        resolutionDropdown.ClearOptions();
        List<string> resolutions = new List<string>();
        for (int i = 0; i < Screen.resolutions.Length; i++)
        {
            resolutions.Add(Screen.resolutions[i].ToString());
        }
        resolutionDropdown.AddOptions(resolutions);

        // restore the saved audio volume
        if (PlayerPrefs.HasKey("AudioVolume"))
        {
            AudioListener.volume =
            PlayerPrefs.GetFloat("AudioVolume");
        }
        else
        {
            // first time the game is run, use the default value
            AudioListener.volume = 1;
        }




    }

    // Update is called once per frame
    void Update () {
       

        timer += Time.deltaTime;
        timer = (Mathf.Round(timer * 100)) / 100.0f;
        score.text = "score:" + "" + timer;
        
        playerHealth.value = playerref.live;
        // pause if the player presses escape
        if (!paused && Input.GetKeyDown(KeyCode.Escape))
        {
            SetPaused(true);
        }

        if (playerref.isplayerAlive == true)
        {
            Time.timeScale = 0f;
            gameover.SetActive(true);
        }
    }

    public void OnPressedPlay()
    {
        info.SetActive(false);
        // resume the game
        SetPaused(false);
    }
    public void OnPressedOptions()
    {
        // show the options panel & hide the shell panel
        shellPanel.SetActive(false);
        optionsPanel.SetActive(true);

        // select the current quality value
        qualityDropdown.value = QualitySettings.GetQualityLevel();

        // select the current resolution
        int currentResolution = 0;
        for (int i = 0; i < Screen.resolutions.Length; i++)
        {
            if (Screen.resolutions[i].width == Screen.width &&
            Screen.resolutions[i].height == Screen.height)
            {
                currentResolution = i;
                break;
            }
        }
        resolutionDropdown.value = currentResolution;

        fullScreenToggle.isOn = Screen.fullScreen;

        // set the volume slider
        volumeSlider.value = AudioListener.volume;
        MusicSlider.value = MusicSource.volume;

    }

    public void OnPressedApply()
    {
        QualitySettings.SetQualityLevel(qualityDropdown.value);
        Resolution res =
 Screen.resolutions[resolutionDropdown.value];
        Screen.SetResolution(res.width, res.height, true);

        // return to the shell menu
        shellPanel.SetActive(true);
        optionsPanel.SetActive(false);

        Screen.SetResolution(res.width, res.height,
 fullScreenToggle.isOn);

        AudioListener.volume = volumeSlider.value;

        PlayerPrefs.SetFloat("AudioVolume", AudioListener.volume);

        MusicSource.volume = MusicSlider.value;
    }

    public void OnPressedCancel()
    {
        // return to the shell menu
        shellPanel.SetActive(true);
        optionsPanel.SetActive(false);
    }

    public void OnPressedQuit()
    {
        // quit the game
        Application.Quit();
    }


    private void SetPaused(bool p)
    {
        
        // make the shell panel (in)active when (un)paused
        paused = p;
        shellPanel.SetActive(paused);
        Time.timeScale = paused ? 0 : 1;
    }
}
