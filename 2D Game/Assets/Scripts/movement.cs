﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(AudioSource))]
public class movement : MonoBehaviour {

    public float speed;
    public float dashSpeed;
     Rigidbody2D rb;
    public int live = 5;
     Animator anim;
    private bool facingRight;
    public bool isplayerAlive = false;
    public AudioClip hurt;
    public AudioClip health;
    private AudioSource audio;

    // Use this for initialization
    void Start () {
        facingRight = true;
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        audio = GetComponent<AudioSource>();
    }
	
	// Update is called once per frame
	void Update () {

       float s;

        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            s = dashSpeed;
        }
        else
        {
            s = speed;
        }
        float x = Input.GetAxis("Horizontal") * s;
        movementHandler(x);
        flip(x);


        if (x==0)
        {
            anim.SetBool("isRuning", false);
        }
        else
        {
            anim.SetBool("isRuning", true);
        }
      
        if (live <= 0)
        {
            GameOver();
        }
    }

    private void movementHandler(float x)
    {
        Vector2 mv = new Vector2(x, rb.velocity.y);
        rb.velocity = mv;
       
    }
    public void flip(float x)
    {
        if (x > 0 && !facingRight|| x < 0 && facingRight)
        {
            facingRight = !facingRight;
            Vector2 scale = transform.localScale;
            scale.x *= -1;
            transform.localScale = scale;
            }
    }

    public void takeDamage(int damage)
    {
        live = live - damage;
        audio.PlayOneShot(hurt);
    }

    public void GetHealth(int health)
    {
        live = live + health;
        audio.PlayOneShot(this.health);
    }

    public void GameOver()
    {
        isplayerAlive = true;
       
    }
}
