﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour {

    public Boomer bomb_prefab;
    public Health heal;
    public int Nbombs;
    public Rect spawnRect;
    public bool isPlayerAlive;
    public int round = 1;

    public float timer = 5;

    // Use this for initialization
    void Start ()
    {
        StartCoroutine(SpawnWaves()); 
    }
	
	// Update is called once per frame
	void Update ()
    {
        timer -= Time.deltaTime;
        if (timer <= 0)
        {
            Health health = Instantiate(heal, this.transform);
            float x = spawnRect.xMin +
                    Random.value * spawnRect.width;
            float y = spawnRect.yMin +
            Random.value * spawnRect.height;
            health.transform.position = new Vector2(x, y);
            timer = 5;
        }
	}

    IEnumerator SpawnWaves()
    {
        while (isPlayerAlive)
        {
            for (int i = 0; i < Nbombs; i++)
            {
                /*if (round / 10 == 1)
                {
                    Quaternion spawnRotation = Quaternion.identity;
                    Boomer bomb = Instantiate(bomb_prefab, this.transform);
                    bomb.speed = 20f;
                    float x = spawnRect.xMin +
                        Random.value * spawnRect.width;
                    float y = spawnRect.yMin +
                    Random.value * spawnRect.height;
                    bomb.transform.position = new Vector2(x, y);
                }
                else
                {*/
                    Quaternion spawnRotation = Quaternion.identity;
                    Boomer bomb = Instantiate(bomb_prefab, this.transform);

                    float x = spawnRect.xMin +
                        Random.value * spawnRect.width;
                    float y = spawnRect.yMin +
                    Random.value * spawnRect.height;
                    bomb.transform.position = new Vector2(x, y);
                    }
                yield return new WaitForSeconds(1f);
            }
            yield return new WaitForSeconds(0.1f);
            round += 1;
        }
    

    void OnDrawGizmos()
    {
        // draw the spawning rectangle
        Gizmos.color = Color.green;
        Gizmos.DrawLine(
         new Vector2(spawnRect.xMin, spawnRect.yMin),
         new Vector2(spawnRect.xMax, spawnRect.yMin));
        Gizmos.DrawLine(
         new Vector2(spawnRect.xMax, spawnRect.yMin),
         new Vector2(spawnRect.xMax, spawnRect.yMax));
        Gizmos.DrawLine(
         new Vector2(spawnRect.xMax, spawnRect.yMax),
         new Vector2(spawnRect.xMin, spawnRect.yMax));
        Gizmos.DrawLine(
         new Vector2(spawnRect.xMin, spawnRect.yMax),
         new Vector2(spawnRect.xMin, spawnRect.yMin));
    }

}
